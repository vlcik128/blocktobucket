package vlcik128.btb.dataf;

import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import vlcik128.btb.data.Settings;

public class BTBPlayerData {
	
	private String name;
	private int uses;
	
	private void dataCheck() {
		if (LocalFileConfiguration.extConfig.getString(name+".tlcUses")==null){
			backToMaxUses();
		}
	}


	private void dataLoad() {
		try{
			this.uses=Integer.parseInt(LocalFileConfiguration.extConfig.getString(name+".tlcUses"));
			
		}catch (NumberFormatException ex) {
			System.out.println("Damaged data file... (tlc.blocktobucketdata:"+name+">>tlcUses)");
			System.out.println("To repair it, use: /btbrepair " + name);
			Bukkit.getPlayer(name).kickPlayer("�bBlockToBucket Error\n�cData file was damaged!\n\n�aJoin back the server \n�eor\n �ccontact the server administrator to send command \n�a�l/btbrepair "+name);
		
			
		}
	}
	public BTBPlayerData(Player p){
		this.name = p.getName();
		dataCheck();
		dataLoad();
		
	}
	public BTBPlayerData(String p){
		this.name = p;
		dataCheck();
		dataLoad();
		
	}
	public void usesDecrementation(){

		LocalFileConfiguration.extConfig.set(name+".tlcUses", uses-1);
		uses--;
		try {
			LocalFileConfiguration.extConfig.save(LocalFileConfiguration.file);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void backToMaxUses(){

		LocalFileConfiguration.extConfig.set(name+".tlcUses", Settings.settings.getTlc().getMax_uses());
		uses=Settings.settings.getTlc().getMax_uses();
		try {
			LocalFileConfiguration.extConfig.save(LocalFileConfiguration.file);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public int getTlcUses(){
		return uses;
	}
	
}
