package vlcik128.btb.dataf;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class LocalFileConfiguration {
	public static FileConfiguration extConfig;
	public static final File file = new File("plugins/BlockToBucket/tlc.blocktobucketdata");
	
	public static void loadConfig(File f){
		if (!f.exists()){
			try {
				System.out.println("File " + f.getPath() + " does not exists, creating a new one!");
				f.createNewFile();
	
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		extConfig = YamlConfiguration.loadConfiguration(f);
		
		
	}
	
}
