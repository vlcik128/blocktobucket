package vlcik128.btb.servcomp.protocols;

import org.bukkit.craftbukkit.v1_11_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

import net.minecraft.server.v1_11_R1.IChatBaseComponent;
import net.minecraft.server.v1_11_R1.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_11_R1.PacketPlayOutChat;
import vlcik128.btb.servcomp.CompatibleVersion;

public class Version1_11_R1 implements CompatibleVersion {

	@Override
	public void sendActionBar(Player player, String message) {
		CraftPlayer p = (CraftPlayer) player;
		IChatBaseComponent cbc = ChatSerializer.a("{\"text\": \"" + message + "\"}");
		PacketPlayOutChat ppoc = new PacketPlayOutChat(cbc,(byte)2);
		((CraftPlayer) p).getHandle().playerConnection.sendPacket(ppoc);

	}

}
