package vlcik128.btb.servcomp.protocols;

import org.bukkit.craftbukkit.v1_9_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;

import net.minecraft.server.v1_9_R2.IChatBaseComponent;
import net.minecraft.server.v1_9_R2.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_9_R2.PacketPlayOutChat;
import vlcik128.btb.servcomp.CompatibleVersion;

public class Version1_9_R2 implements CompatibleVersion {

	@Override
	public void sendActionBar(Player player, String message) {
		CraftPlayer p = (CraftPlayer) player;
		IChatBaseComponent cbc = ChatSerializer.a("{\"text\": \"" + message + "\"}");
		PacketPlayOutChat ppoc = new PacketPlayOutChat(cbc,(byte)2);
		((CraftPlayer) p).getHandle().playerConnection.sendPacket(ppoc);
		
	}

}
