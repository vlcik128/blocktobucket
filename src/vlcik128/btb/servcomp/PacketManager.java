package vlcik128.btb.servcomp;

import java.util.ArrayList;
import java.util.List;

public class PacketManager {

	public static CompatibleVersion actualVersion;
	public static List<String> compatibleNMSVersionNamesToServerVersionComparedNamesList;
	private static void initCompatibleNMSVersionNamesToServerVersionComparedNamesList(){
		compatibleNMSVersionNamesToServerVersionComparedNamesList=new ArrayList<String>();
		List<String> ins = compatibleNMSVersionNamesToServerVersionComparedNamesList;
		ins.add("1.8.4");
		ins.add("1.8.5");
		ins.add("1.8.6");
		ins.add("1.8.7");
		ins.add("1.8.8");
		ins.add("1.9");
		ins.add("1.9.2");
		ins.add("1.9.4");
		ins.add("1.10");
		ins.add("1.10.2");
		ins.add("1.11");
		ins.add("1.11.1");
		ins.add("1.11.2");
		ins.add("1.12");
		ins.add("1.12.1");
		ins.add("1.12.2");
		
	}
	public static String cnmsvntsvcmLIST(){
		initCompatibleNMSVersionNamesToServerVersionComparedNamesList();
		String product = "";
		for (String s: compatibleNMSVersionNamesToServerVersionComparedNamesList){
			product+=s+", ";
		}
		return product;
	}
	public static void newCompatibleVersion(CompatibleVersion ver){
		initCompatibleNMSVersionNamesToServerVersionComparedNamesList();
		actualVersion=ver;
	}
	
}
