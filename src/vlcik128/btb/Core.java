package vlcik128.btb;

import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import net.md_5.bungee.api.ChatColor;
import vlcik128.btb.data.Settings;
import vlcik128.btb.dataf.JPrepare;
import vlcik128.btb.dataf.LocalFileConfiguration;
import vlcik128.btb.parts.itw.LIceToWater;
import vlcik128.btb.parts.otl.LObsidianToLava;
import vlcik128.btb.parts.tlc.LPackedIceToIce;
import vlcik128.btb.servcomp.PacketManager;
import vlcik128.btb.servcomp.protocols.Version1_10_R1;
import vlcik128.btb.servcomp.protocols.Version1_11_R1;
import vlcik128.btb.servcomp.protocols.Version1_12_R1;
import vlcik128.btb.servcomp.protocols.Version1_8_R3;
import vlcik128.btb.servcomp.protocols.Version1_9_R1;
import vlcik128.btb.servcomp.protocols.Version1_9_R2;

public class Core extends JavaPlugin {
	
	public static Core plugin;
	public static FileConfiguration cfg;

	
	//Permissions:
	/*
	 * - btb.repair
	 * - btb.pti
	 * - btb.itw
	 * - btb.otl
	 * 
	 */
	public void onEnable(){
		plugin=this;
		setCompatibleServerVersion();
		saveDefaultConfig();
		LocalFileConfiguration.loadConfig(LocalFileConfiguration.file);
		cfg=getConfig();
		
		getServer().getPluginManager().registerEvents(new JPrepare(), this);
		Settings.load();
		getCommand("btbrepair").setExecutor(this);
		setupPluginParts();
	}
	
	public void onDisable(){
		// DEBILINA Settings.save();
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if (label.equalsIgnoreCase("btbrepair")){
			if (sender instanceof Player&&sender.hasPermission("btb.repair")){
				if (args.length==0){
					sender.sendMessage(ChatColor.AQUA+"BTB> §cUsage: §6/btbrepair <player>");
				}else if (args.length==1){
					String n = args[0];
					LocalFileConfiguration.extConfig.set(n+".tlcUses", Settings.settings.getTlc().getMax_uses());
					try {
						LocalFileConfiguration.extConfig.save(LocalFileConfiguration.file);
						sender.sendMessage(ChatColor.AQUA+"BTB> §aData for " + n + " was successfully repaired!");
					} catch (IOException e) {
						sender.sendMessage(ChatColor.AQUA+"BTB> §cAn error occured while attempting to repair " + n + "'s data!");
						e.printStackTrace();
					}
					
				}
			    
			}else if (!(sender instanceof Player)){
				if (args.length==0){
					sender.sendMessage(ChatColor.AQUA+"BTB> §cUsage: §6/btbrepair <player>");
				}else if (args.length==1){
					String n = args[0];
					LocalFileConfiguration.extConfig.set(n+".tlcUses", Settings.settings.getTlc().getMax_uses());
					sender.sendMessage(ChatColor.AQUA+"BTB> §aData for " + n + " was successfully repaired!");
					try {
						LocalFileConfiguration.extConfig.save(LocalFileConfiguration.file);
					} catch (IOException e) {
						sender.sendMessage(ChatColor.AQUA+"BTB> §cAn error occured while attempting to repair " + n + "'s data!");
						e.printStackTrace();
					}
					
				}
				
			}
		}
		return false;
	}
	
	private void setupPluginParts() {
		setupTripleLavaClick();
		setupIceToWater();
		setupObsidianToLava();
		
	}



	private void setupObsidianToLava() {
		getServer().getPluginManager().registerEvents(new LObsidianToLava(), this);
		
	}



	private void setupIceToWater() {
		getServer().getPluginManager().registerEvents(new LIceToWater(), this);
		
	}



	private void setupTripleLavaClick() {
		getServer().getPluginManager().registerEvents(new LPackedIceToIce(), this);
		
	}



	private void setCompatibleServerVersion(){
		String version = Bukkit.getServer().getClass().getPackage().getName();
		String formattedVersion = version.substring(version.lastIndexOf('.') + 1);
		
		switch (formattedVersion){
		case "v1_8_R3":
			PacketManager.newCompatibleVersion(new Version1_8_R3());
			break;
		case "v1_9_R1":
			PacketManager.newCompatibleVersion(new Version1_9_R1());
			break;
		case "v1_9_R2":
			PacketManager.newCompatibleVersion(new Version1_9_R2());
			break;
		case "v1_10_R1":
			PacketManager.newCompatibleVersion(new Version1_10_R1());
			break;
		case "v1_11_R1":
			PacketManager.newCompatibleVersion(new Version1_11_R1());
			break;
		case "v1_12_R1":
			PacketManager.newCompatibleVersion(new Version1_12_R1());
			break;
		default: 
			incompatibleVersion();
		}
	}
	
	
	private void incompatibleVersion(){
		System.out.println("You are using not supported version of your Spigot server, you can only run this plugin on mc server version: \n"+		PacketManager.cnmsvntsvcmLIST());
		getLogger().severe("Disabling plugin");	
		getServer().getPluginManager().disablePlugin(this);
	}
	

}
