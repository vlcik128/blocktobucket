package vlcik128.btb.parts.itw;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import vlcik128.btb.Core;
import vlcik128.btb.data.Settings;
import vlcik128.btb.servcomp.PacketManager;

public class LIceToWater implements Listener {

	@EventHandler
	public void in(PlayerInteractEvent e){
		if (!Settings.settings.getItw().isEnabled())return;
		if (!e.getPlayer().hasPermission("btb.itw"))return;
		Player p = e.getPlayer();
		Block b = e.getClickedBlock();
		Action a = e.getAction();
		ItemStack is = e.getPlayer().getItemInHand();
		if (p==null) return;
		if (b==null||b.getType()==null||b.getType()!= Material.ICE)return;
		if (a==null||a!=Action.RIGHT_CLICK_BLOCK)return;
		if (is==null||is.getType()==null||is.getType()!=Material.BUCKET) return;
	
		p.getInventory().removeItem(is);
		b.setType(Material.AIR);
		PacketManager.actualVersion.sendActionBar(p, Settings.settings.getItw().getMessage().replace('&', '�'));
		new BukkitRunnable(){
			public void run(){
				p.getInventory().addItem(new ItemStack(Material.WATER_BUCKET));
			}
		}.runTaskLater(Core.plugin, 2L);
	}
}
