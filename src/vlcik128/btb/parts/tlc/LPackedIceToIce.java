package vlcik128.btb.parts.tlc;



import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import vlcik128.btb.data.FSettings;
import vlcik128.btb.data.Settings;
import vlcik128.btb.dataf.BTBPlayerData;
import vlcik128.btb.servcomp.PacketManager;

public class LPackedIceToIce implements Listener{

	@EventHandler
	public void interact(PlayerInteractEvent e){
		/*	 boolean newCombat;*/
		if (!Settings.settings.getTlc().isEnabled())return; // Skontrolovat neskor..
		Player p = e.getPlayer();
		Block b = e.getClickedBlock();
		Action a = e.getAction();
		ItemStack is = e.getPlayer().getItemInHand();
		if (p==null) return;
		if (b==null || b.getType()==null)return;
		if (a==null || a==Action.PHYSICAL) return;
		if (is==null || is.getType()==null || is.getType()!=Material.LAVA_BUCKET) return;
		if (!p.hasPermission("btb.pti"))return;
		BTBPlayerData data = new BTBPlayerData(p);
		//DEBUG	System.out.println(data.getTlcUses());
		
		
		// nezabudnut, ze to je %uses-left% a nie %max-uses% - HOTOVO
		// opravit diagram - HOTOVO
		PacketManager.actualVersion.sendActionBar(p, Settings.settings.getTlc().getMessage().replace("%diagram%", FSettings.diagram(p.getName())).replace("%uses-left%", data.getTlcUses()+"").replace('&', '�'));
		if (b.getType()==Material.PACKED_ICE){
			data.usesDecrementation();
			b.setType(Material.ICE);
			PacketManager.actualVersion.sendActionBar(p, Settings.settings.getTlc().getMessage().replace("%diagram%", FSettings.diagram(p.getName())).replace("%uses-left%", data.getTlcUses()+"").replace('&', '�'));
			if (data.getTlcUses()==0){
				p.getInventory().removeItem(is);
				p.getInventory().addItem(new ItemStack(Material.BUCKET));
				data.backToMaxUses();
				
			}

		}
	}
}
