package vlcik128.btb.data;

import vlcik128.btb.Core;

public class TLC {

	private boolean enabled;
	private String message;
	private String diagram_true;
	private String diagram_false;
	private int max_uses;
	
	public TLC(boolean enabled, String message, String diagram_true, String diagram_false, String max_uses){
		this.setEnabled(enabled);
		this.setMessage(message);
		this.setDiagram_false(diagram_false);
		this.setDiagram_true(diagram_true);
		try{
			this.setMax_uses(Integer.parseInt(max_uses));
		}catch (NumberFormatException ex){
			System.out.println("NumberFormatException: Invalid config.yml at packedice-to-ice>>max_uses");
			System.out.println("Disabling plugin!");
			Core.plugin.getServer().getPluginManager().disablePlugin(Core.plugin);
		}
		
		
	}

	public String getDiagram_false() {
		return diagram_false;
	}

	public void setDiagram_false(String diagram_false) {
		this.diagram_false = diagram_false;
	}

	public int getMax_uses() {
		return max_uses;
	}

	public void setMax_uses(int max_uses) {
		this.max_uses = max_uses;
	}

	public String getDiagram_true() {
		return diagram_true;
	}

	public void setDiagram_true(String diagram_true) {
		this.diagram_true = diagram_true;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	
}
