package vlcik128.btb.data;

import vlcik128.btb.Core;

public class Settings {

	public static PSettings settings;
	private static String pathForObject(String p, String c){
		return p+"."+c;
	}
	
	public static void load(){
		ITW itw;
		OTL otl;
		TLC tlc;
		
		final String itwp = "ice-to-water";
		final String otlp = "obsidian-to-lava";
		final String e = "enabled";
		final String m = "message";
		
		boolean tlc_on = Core.cfg.getBoolean(pathForObject("packedice-to-ice", "enabled"));
		String tlc_m = Core.cfg.getString(pathForObject("packedice-to-ice", "message"));
		String tlc_d0 = Core.cfg.getString(pathForObject("packedice-to-ice", "diagram-false"));
		String tlc_d1 = Core.cfg.getString(pathForObject("packedice-to-ice", "diagram-true"));
		String tlc_max = Core.cfg.getString(pathForObject("packedice-to-ice", "max-uses"));
		
		boolean itw_on = Core.cfg.getBoolean(pathForObject(itwp, e));
	    String itw_m = Core.cfg.getString(pathForObject(itwp, m));
	    
	    boolean otl_on = Core.cfg.getBoolean(pathForObject(otlp,e));
	    String otl_m = Core.cfg.getString(pathForObject(otlp,m));
	    
	    itw = new ITW(itw_on,itw_m);
	    otl = new OTL(otl_on, otl_m);
	    tlc = new TLC(tlc_on,tlc_m,tlc_d1,tlc_d0,tlc_max);
	    settings= new PSettings(itw,otl,tlc);
	}
	
	public static void save(){
		final String pti = "packedice-to-ice";
		final String itwp = "ice-to-water";
		final String otlp = "obsidian-to-lava";
		final String e = "enabled";
		final String m = "message";
		final String d0 = "diagram-false";
		final String d1 = "diagram-true";
		final String max = "max-uses";
		
		
		
		boolean tlc_on = settings.getTlc().isEnabled();
		boolean itw_on = settings.getItw().isEnabled();
		boolean otl_on = settings.getOtl().isEnabled();
		String tlc_m = settings.getTlc().getMessage();
		String tlc_d0 = settings.getTlc().getDiagram_false();
		String tlc_d1 = settings.getTlc().getDiagram_true();
		String tlc_max = String.valueOf(settings.getTlc().getMax_uses());
		String otl_m = settings.getOtl().getMessage();
		String itw_m = settings.getItw().getMessage();
		
		Core.cfg.set(pathForObject(pti, e), tlc_on);
		Core.cfg.set(pathForObject(itwp, e), itw_on);
		Core.cfg.set(pathForObject(otlp, e), otl_on);

		Core.cfg.set(pathForObject(pti, m), tlc_m);
		Core.cfg.set(pathForObject(pti, d0),tlc_d0);
		Core.cfg.set(pathForObject(pti, d1), tlc_d1);
		Core.cfg.set(pathForObject(pti, max), tlc_max);
		Core.cfg.set(pathForObject(otlp, m), otl_m);
		Core.cfg.set(pathForObject(itwp, m), itw_m);
	
		Core.plugin.saveConfig();
	}
	
	

}
