package vlcik128.btb.data;

public class PSettings {

	private ITW itw;
	private OTL otl;
	private TLC tlc;
	
	public PSettings(ITW itw, OTL otl, TLC tlc){
		this.setItw(itw);
		this.setOtl(otl);
		this.setTlc(tlc);
	}

	public TLC getTlc() {
		return tlc;
	}

	public void setTlc(TLC tlc) {
		this.tlc = tlc;
	}

	public OTL getOtl() {
		return otl;
	}

	public void setOtl(OTL otl) {
		this.otl = otl;
	}

	public ITW getItw() {
		return itw;
	}

	public void setItw(ITW itw) {
		this.itw = itw;
	}
}
