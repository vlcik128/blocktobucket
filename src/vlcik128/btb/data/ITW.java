package vlcik128.btb.data;

public class ITW {
	
	private boolean enabled;
	private String message;
	
	public ITW(boolean enabled, String message){
		this.setEnabled(enabled);
		this.setMessage(message);
		
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

}
